package io.github.example.myfinaltictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnStart;
    private Button btnScoreBoard;
    private Button btnPlayerSelect;
    private Button btnAddPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStart = (Button) findViewById(R.id.btnStart);
        btnScoreBoard = (Button) findViewById(R.id.btnScoreboard);
        btnPlayerSelect = (Button) findViewById(R.id.btnPlayerSelect);
        btnAddPlayer = (Button) findViewById(R.id.btnAddPlayer);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToStartGameActivity();
            }
        });
        btnAddPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToAddPlayer();
            }
        });
        btnScoreBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToScoreBoard();
            }
        });


        btnPlayerSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToPlayerSelect();
            }
        });

    }

    @Override
    public void onClick(View v) {

    }

    private void moveToStartGameActivity(){
        Intent intent = new Intent(MainActivity.this,StartGameActivity.class);
        startActivity(intent);
    }
    private void moveToAddPlayer(){
        Intent intent = new Intent(MainActivity.this,AddPlayer.class);
        startActivity(intent);
    }
    private void moveToPlayerSelect(){
        Intent intent = new Intent(MainActivity.this,SelectPlayerActivity.class);
        startActivity(intent);
    }
    private void moveToScoreBoard(){
        Intent intent = new Intent(MainActivity.this,ScoreboardActivity.class);
        startActivity(intent);
    }

}
