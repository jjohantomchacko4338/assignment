package io.github.example.myfinaltictactoe;

import java.util.HashMap;

public class Player extends HashMap<String, String> {
    @Override
    public String toString(){
        return this.get("name");
    }
    public String wintoString(){
        return this.get("wins");
    }
    public String losstoString(){
        return this.get("losses");
    }
    public String drawtoString(){ return this.get("draws"); }
}
