package io.github.example.myfinaltictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;
import java.util.jar.Attributes;

import androidx.appcompat.app.AppCompatActivity;

public class SelectPlayerActivity<baseAdapter> extends AppCompatActivity  {


    public TextView text_view_player1;
    public TextView text_view_player2;
    public Spinner player1_spin;
    public Spinner player2_spin;
    public Button btnPlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_player);

        text_view_player1 = findViewById(R.id.text_view_player1);
        text_view_player2 = findViewById(R.id.text_view_player2);

        player1_spin = (Spinner) findViewById(R.id.player1_spin);
        player2_spin = (Spinner) findViewById(R.id.player2_spin);
        btnPlay = (Button) findViewById(R.id.btnPlay);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToStartGameActivity();
            }
        });

        SelectPlayerFragment fragment = (SelectPlayerFragment) getSupportFragmentManager().findFragmentById(R.id.main_fragment);
        fragment.restart();


    }
    private void moveToStartGameActivity(){
        Intent intent = new Intent(SelectPlayerActivity.this,StartGameActivity.class);
        startActivity(intent);
    }
}