package io.github.example.myfinaltictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class ScoreboardActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView itemsListView;
    private EditText oNameEdit;
    private Button oNameInsert;
    private PlayerDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);

        itemsListView = (ListView) findViewById(R.id.itemsListView);


        db = new PlayerDB(this);
        updateDisplay();

    }

    @Override
    public void onClick(View v){

    }

    private void updateDisplay(){
        // create a List of Map<String, ?> objects
        ArrayList<Player> data = db.getPlayers();

        // create the resource, from, and to variables
        int resource = R.layout.listview_item;
        String[] from = {"name", "wins", "losses", "ties"};
        int[] to = {R.id.nameTextView, R.id.winsTextView, R.id.lossesTextView, R.id.tiesTextView};

        // create and set the adapter
        SimpleAdapter adapter =
                new SimpleAdapter(this, data, resource, from, to);
        itemsListView.setAdapter(adapter);

    }
}
